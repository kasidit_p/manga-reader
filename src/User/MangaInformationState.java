package User;

import UI.MainMenuUI;

/**
 * This state will change centerpanel in MainMenuUI to mangainformation.
 * @author Kasidit Phoncharoen and Sorawit Sakulkalanuwat
 *
 */
public class MangaInformationState extends UserState{

	public MangaInformationState(User user) {
		super(user);
	}

	@Override
	public void updateUI() {
		MainMenuUI.getPanelCenter().removeAll();
	}

}
