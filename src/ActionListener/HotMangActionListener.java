package ActionListener;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import User.User;

/**
 * ActionListener for hot label to change to hot page.
 */
public class HotMangActionListener implements MouseListener {
	
	private JLabel label;
	
	/**
	 * Constructor for this class.
	 * @param label
	 */
	public HotMangActionListener(JLabel label){
		this.label = label;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		User user = User.getInstance();
		user.setState(user.HOTMANGA_STATE);
		user.update();
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		if(label != null) label.setIcon(new ImageIcon(ClassLoader.getSystemResource("images/Hot1.jpg")));
	}

	@Override
	public void mouseExited(MouseEvent e) {
		if(label != null) label.setIcon(new ImageIcon(ClassLoader.getSystemResource("images/Hot.jpg")));
	}

}