package downloader;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;

import ActionListener.DownloadButtonActionListener;
import ActionListener.ReadActionListener;
import Manga.Chapter;
import Manga.Manga;
import UI.MainMenuUI;
import UI.ReadMangaUI;
import User.User;

/**
 * This class will download information to display in manga information page.
 * @author Kasidit Phoncharoen and Sorawit Sakulkalanuwat
 *
 */
public class DownloadInformation extends SwingWorker < Integer, Integer > implements Runnable{
	
	private int countProgess = 0;
	private int totalInformation = 5;
	private Manga manga;
	private Chapter chapter;
	private JTextField chapterNumText;

	/**
	 * Constructor for this class.
	 * @param manga that want to get information.
	 */
	public DownloadInformation(Manga manga ){
		this.manga = manga;
	}
	
	/**
	 * This method will get information of manga in backgroung using SwingWorker.
	 */
	protected Integer doInBackground() throws Exception {
		MainMenuUI.getPanelCenter().setLayout(new BorderLayout());
		MainMenuUI.panelSearch.setVisible(false);
		JPanel panelTop = new JPanel();
		panelTop.setBackground(Color.GRAY);
		panelTop.setLayout(new FlowLayout(FlowLayout.LEFT));
		JPanel panelTopLeft = new JPanel();
		JLabel label = null;
		Image image = null;
		try {
			image = ImageIO.read(manga.getCoverURL());
			Image newimg = image.getScaledInstance(100, 100,  java.awt.Image.SCALE_SMOOTH);
			ImageIcon newIcon = new ImageIcon(newimg);
			label = new JLabel(newIcon);
			panelTopLeft.add(label);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		countProgess++;
		publish(countProgess);
		panelTop.add(panelTopLeft);
		JPanel panelTopMid = new JPanel();
		panelTopMid.setLayout(new BoxLayout(panelTopMid, BoxLayout.Y_AXIS));
		countProgess++;
		publish(countProgess);
		if(manga.getName().length() > 15){
			JLabel name1 = new JLabel(manga.getName().substring(0, 15));
			panelTopMid.add(name1);
			JLabel name2 = new JLabel(manga.getName().substring(15, manga.getName().length()));
			panelTopMid.add(name2);
		} else {
			JLabel name = new JLabel(manga.getName());
			panelTopMid.add(name);
		}
		countProgess++;
		publish(countProgess);
		JLabel space = new JLabel(" ");
		panelTopMid.add(space);
		JLabel chapter = new JLabel("Total  :   "+manga.getTotalChapter()+"  chapters");
		panelTopMid.add(chapter);
		JLabel status = new JLabel("Status  :  " + (manga.getCompleted() ? "Completed" : "On going"));
		panelTopMid.add(status);
		countProgess++;
		publish(countProgess);
		panelTop.add(panelTopMid);
		JPanel panelTopRight = new JPanel();
		JTextArea summary = new JTextArea(7,27);
		summary.setLineWrap(true);
		summary.setWrapStyleWord(true);
		JScrollPane scrollSummary = new JScrollPane(summary);
		scrollSummary.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		summary.setText(manga.getSummary());
		countProgess++;
		publish(countProgess);
		panelTopRight.add(scrollSummary);
		panelTop.add(panelTopRight);
		//Button panel for button read and download by chapter number
		JPanel panelButtonTop = new JPanel();
		panelButtonTop.setLayout(new BoxLayout(panelButtonTop,BoxLayout.Y_AXIS));
		chapterNumText = new JTextField(5);
		panelButtonTop.add(chapterNumText);
		JButton readNowButton = new JButton("Read");
		readNowButton.addMouseListener(new ReadByNumActionListener());
		panelButtonTop.add(readNowButton);
		JButton downloadNowButton = new JButton("Download");
		downloadNowButton.addMouseListener(new DownloadByNumActionListener());
		panelButtonTop.add(downloadNowButton);
		panelTop.add(panelButtonTop);

		JPanel panelBot = new JPanel();
		JScrollPane scrollPane = new JScrollPane(panelBot, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.getVerticalScrollBar().setUnitIncrement(16);
		panelBot.setLayout(new BoxLayout(panelBot, BoxLayout.Y_AXIS));
		List<Chapter> list = manga.getChapterList();
		totalInformation += list.size();
		publish(countProgess);
		for(Chapter a : list){
			countProgess++;
			publish(countProgess);
			JPanel panel = new JPanel();
			panel.setLayout(new GridLayout());
			JLabel chapterLine = new JLabel(String.format("%s %-3s  :  %-40s", (manga.getName().length() > 15 ? manga.getName().substring(0, 15)+"..." : manga.getName()) , a.getNumChapter(), (a.getName().length() > 35 ? a.getName().substring(0, 35)+"..." : a.getName()) ));
			panel.add(chapterLine);
			JPanel panelButton = new JPanel();
			JButton readButton = new JButton("Read Online");
			readButton.addMouseListener(new ReadActionListener(a));
			panelButton.add(readButton);
			JButton downloadButton = new JButton("Download");
			downloadButton.addMouseListener(new DownloadButtonActionListener(manga,a));
			panelButton.add(downloadButton);
			panel.add(panelButton);
			panelBot.add(panel);
		}

		MainMenuUI.getPanelCenter().add(panelTop, BorderLayout.NORTH);
		MainMenuUI.getPanelCenter().add(scrollPane, BorderLayout.CENTER);
		panelTop.repaint();
		MainMenuUI.instance.pack();
		MainMenuUI.instance.repaint();

		return countProgess;
	}

	/**
	 * This mehtod will do when doInbackground done.
	 * This will set download bar in gui to be 0.
	 */
	public void done(){
		MainMenuUI.getDownloadBar().setValue(0);
	}

	/**
	 * This method 
	 */
	protected void process(List<Integer> chunks) {
		super.process(chunks);
		MainMenuUI.getDownloadBar().setMaximum(totalInformation);
		MainMenuUI.getDownloadBar().setValue(chunks.get(chunks.size()-1));
	}
	
	/**
	 * ActionListener for download by number of chapter.
	 */
	class DownloadByNumActionListener implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {
			int numChap = Integer.parseInt(chapterNumText.getText());
			if(numChap <= manga.getTotalChapter()){
				chapter = manga.getChapter(numChap);
				SwingUtilities.invokeLater( new Runnable( ) {
					public void run() {
						DownloadManga download = new DownloadManga(manga, chapter);
						download.execute();
					}
				});
			}
		}

		@Override
		public void mousePressed(MouseEvent e) {
		}

		@Override
		public void mouseReleased(MouseEvent e) {
		}

		@Override
		public void mouseEntered(MouseEvent e) {
		}

		@Override
		public void mouseExited(MouseEvent e) {
		}

	}

	/**
	 * ActionListener for read button by number chapter. 
	 */
	class ReadByNumActionListener implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {
			int numChap = Integer.parseInt(chapterNumText.getText());
			if(numChap <= manga.getTotalChapter())
				try {
					User.getInstance().setState(User.getInstance().READING_STATE);
					User.getInstance().update();
					chapter = manga.getChapter(numChap);
					ReadMangaUI ui = new ReadMangaUI(chapter.getURLList(), chapter);
					ui.run();
				} catch (IOException e1) {
					chapterNumText.setText("Please enter correct number");
					MainMenuUI.getInstance().setVisible(false);
				}
		}

		@Override
		public void mousePressed(MouseEvent e) {
		}

		@Override
		public void mouseReleased(MouseEvent e) {
		}

		@Override
		public void mouseEntered(MouseEvent e) {
		}

		@Override
		public void mouseExited(MouseEvent e) {
		}

	}

}