package ActionListener;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import UI.MainMenuUI;
import UI.ReadMangaOfflineUI;

/**
 * ActionListener for Help label.
 */
public class HelpActionListener implements MouseListener{
	
	private JLabel label;
	
	/**
	 * Constructor for this class.
	 * @param label
	 */
	public HelpActionListener(JLabel label){
		this.label = label;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		MainMenuUI.getInstance().setVisible(false);
		File helpFile = new File(ClassLoader.getSystemResource("images/help/").getPath());
		ReadMangaOfflineUI ui = new ReadMangaOfflineUI(helpFile.listFiles(), "Help Page");
		try {
			ui.run();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		label.setIcon(new ImageIcon(ClassLoader.getSystemResource("images/Help2.jpg")));
	}

	@Override
	public void mouseExited(MouseEvent e) {
		label.setIcon(new ImageIcon(ClassLoader.getSystemResource("images/Help.jpg")));
	}

}
