package ActionListener;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.SwingUtilities;

import downloader.DownloadManga;
import Manga.Chapter;
import Manga.Manga;

/**
 * ActionListener for download button.
 */
public class DownloadButtonActionListener implements MouseListener {

	private Manga manga;
	private Chapter chapter;
	
	/**
	 * Constructor foe this class.
	 * @param manga 
	 * @param chapter
	 */
	public DownloadButtonActionListener(Manga manga, Chapter chapter){
		this.manga = manga;
		this.chapter = chapter;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		SwingUtilities.invokeLater( new Runnable( ) {
			public void run() {
				DownloadManga a = new DownloadManga(manga,chapter);
				a.execute();
			}
		});
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
	}
	
	@Override
	public void mouseReleased(MouseEvent e) {
	}
	
	@Override
	public void mouseEntered(MouseEvent e) {
	}
	
	@Override
	public void mouseExited(MouseEvent e) {
	}
}
