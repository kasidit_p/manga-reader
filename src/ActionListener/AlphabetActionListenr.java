package ActionListener;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import Manga.Manga;
import UI.MainMenuUI;
import User.User;

/**
 * ActionListener for each alphabet in Catlog. 
 */
public class AlphabetActionListenr implements MouseListener{

	private char alphabet;
	private JLabel label;

	public AlphabetActionListenr(char alphabet, JLabel label){
		this.alphabet = alphabet;
		this.label = label;
	}
	
	/**
	 * Update ui to the result from click alphabet.
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		List<Manga> list = User.getInstance().getURLManga().getMangaByAlphabet(alphabet);
		JPanel temp = new JPanel();
		temp.setBackground(Color.BLACK);
		temp.setLayout(new BoxLayout(temp, BoxLayout.Y_AXIS));
		for(int i = 0 ; i < list.size() ; i++ ){
			Manga manga = list.get(i);
			JLabel labelManga = new JLabel(manga.getName());
			labelManga.setForeground(Color.WHITE);
			labelManga.addMouseListener(new MangaActionListener(manga, labelManga, false));
			temp.add(labelManga);
		}
		
		CatalogActionListener tempAction = new CatalogActionListener(label);
		tempAction.mouseClicked(e);
		
		JScrollPane pane = new JScrollPane(temp);
		pane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		MainMenuUI.getPanelCenter().add(pane, BorderLayout.CENTER);
		MainMenuUI.getInstance().pack();
		MainMenuUI.getInstance().repaint();
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		label.setForeground(Color.BLUE);
	}

	@Override
	public void mouseExited(MouseEvent e) {
		label.setForeground(Color.BLACK);
	}

}