package ActionListener;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import UI.MainMenuUI;
import User.User;


/**
 * ActionListener for home button. When click it will change the part of the screen to Home screen.
 */
public class HomeActionListener implements MouseListener {

	@Override
	public void mouseClicked(MouseEvent e) {
		MainMenuUI.getDownloadBar().setValue(0);
		MainMenuUI.getDownloadBar().setString("");
		MainMenuUI.getPanelCenter().removeAll();
		User.getInstance().setState(User.getInstance().SELECT_STATE);
		MainMenuUI.getPanelCenter().setLayout(new BoxLayout(MainMenuUI.getPanelCenter(), BoxLayout.Y_AXIS));
		JPanel firstRow = new JPanel();
		firstRow.setBackground(Color.WHITE);
		firstRow.setLayout(new GridLayout());
		JLabel hotLabel = new JLabel(new ImageIcon(ClassLoader.getSystemResource("images/Hot.jpg")));
		firstRow.add(hotLabel);
		hotLabel.addMouseListener(new HotMangActionListener(hotLabel));
		JLabel catalogLabel = new JLabel(new ImageIcon(ClassLoader.getSystemResource("images/Catalog.jpg")));
		catalogLabel.addMouseListener(new CatalogActionListener(catalogLabel));
		firstRow.add(catalogLabel);
		JPanel secondRow =new JPanel();
		secondRow.setBackground(Color.WHITE);
		secondRow.setLayout(new GridLayout());
		JLabel downloadLabel = new JLabel(new ImageIcon(ClassLoader.getSystemResource("images/Download.jpg")));
		downloadLabel.addMouseListener(new DownloadActionListener(downloadLabel));
		secondRow.add(downloadLabel);
		JLabel helpLabel = new JLabel(new ImageIcon(ClassLoader.getSystemResource("images/Help.jpg")));
		helpLabel.addMouseListener(new HelpActionListener(helpLabel));
		secondRow.add(helpLabel);
		MainMenuUI.getPanelCenter().add(firstRow);
		MainMenuUI.getPanelCenter().add(secondRow);
		MainMenuUI.getInstance().pack();
		MainMenuUI.getInstance().repaint();
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

}