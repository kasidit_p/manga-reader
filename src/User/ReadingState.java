package User;

import UI.MainMenuUI;

/**
 * This state will start MainMenuUI
 * @author Kasidit Phoncharoen and Sorawit Sakulkalanuwat
 *
 */
public class ReadingState extends UserState{

	public ReadingState(User user) {
		super(user);
	}

	@Override
	public void updateUI() {
		MainMenuUI.instance.setVisible(false);
	}

}
