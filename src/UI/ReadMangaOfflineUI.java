package UI;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import User.User;


/**
 * This class is for read offline. It will read downloaded file in computer.
 * @author Kasidit Phoncharoen and Sorawit Sakulkalanuwat
 *
 */
public class ReadMangaOfflineUI extends JFrame{
	private List<URL> listURL = new ArrayList<URL>();
	private int numPage = 0;
	private JScrollPane scroller;
	private ReadMangaOfflineUI ui;
	private JLabel label;
	private JLabel labelNumPage;
	private JPanel panelTopMaster;
	private List<Image> imageArray;
	private File[] imagePath;
	
	/**
	 * Constructor for this class.
	 * @param imagePath 
	 * @param name
	 */
	public ReadMangaOfflineUI(File[] imagePath, String name)  {
		super(name);
		this.imagePath = imagePath;
		ui = this;
		imageArray = new ArrayList<Image>();
	}

	/**
	 * This is component of this ui.
	 * @throws IOException
	 */
	public void initComponent() throws IOException{
		getImgage();
		super.setLayout(new BoxLayout(super.getContentPane() , BoxLayout.Y_AXIS));

		panelTopMaster = new JPanel();
		panelTopMaster.setLayout(new FlowLayout(FlowLayout.LEFT));
		panelTopMaster.setBackground(Color.BLACK);
		JPanel panelTop = new JPanel();
		panelTop.setBackground(Color.BLACK);
		JPanel panelTop2 = new JPanel();
		panelTop2.setBackground(Color.BLACK);
		JPanel panelTop3 = new JPanel();
		panelTop3.setBackground(Color.BLACK);
		JPanel panelTop4 = new JPanel();
		panelTop4.setBackground(Color.BLACK);
		JPanel panelTop5 = new JPanel();
		panelTop5.setBackground(Color.BLACK);
		JPanel panelTop6 = new JPanel();
		panelTop6.setBackground(Color.BLACK);
		JLabel labelIcon = new JLabel(new ImageIcon(ClassLoader.getSystemResource("images/MangaReader3.png")));
		panelTop.add(labelIcon);
		JLabel labelHome = new JLabel(new ImageIcon(ClassLoader.getSystemResource("images/Home2.png")));
		labelHome.addMouseListener(new HomeActionListener());
		panelTop2.add(labelHome);
		JLabel labelLeftArrow = new  JLabel(new ImageIcon(ClassLoader.getSystemResource("images/Left-Arrow.png")));
		labelLeftArrow.addMouseListener(new LeftArrowActionListen());
		panelTop4.add(labelLeftArrow);
		JLabel labelRightArrow = new  JLabel(new ImageIcon(ClassLoader.getSystemResource("images/Right-Arrow.png")));
		labelRightArrow.addMouseListener(new RightArrowActionListen());
		panelTop5.add(labelRightArrow);
		labelNumPage = new JLabel(String.format("Page : %-2d/%-2d", (numPage+1), imageArray.size()));
		labelNumPage.setForeground(Color.WHITE);
		labelNumPage.setFont(new Font("Tahoma", Font.BOLD, 60));
		panelTop6.add(labelNumPage);
		JPanel panelTop7 = new JPanel();
		panelTop7.setBackground(Color.BLACK);
		JLabel tempLabel = new JLabel("                ");
		panelTop7.add(tempLabel);

		panelTopMaster.add(panelTop);
		panelTopMaster.add(panelTop2);
		panelTopMaster.add(panelTop3);
		panelTopMaster.add(panelTop4);
		panelTopMaster.add(panelTop5);
		panelTopMaster.add(panelTop6);
		panelTopMaster.add(panelTop7);

		label = new JLabel(new ImageIcon(imageArray.get(0)));
		JPanel panel = new JPanel();
		panel.add(label);
		scroller = new JScrollPane(panel);
		scroller.setAutoscrolls(true);
		scroller.getVerticalScrollBar().setUnitIncrement(10);
		
		super.addKeyListener(new KeyControlListener());
		super.add(panelTopMaster);
		super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		super.add(scroller);
		super.pack();
		super.setLocation(200, 200);
		super.setVisible(true);
	}
	
	class HomeActionListener implements MouseListener{

		@Override
		public void mouseClicked(MouseEvent e) {
			ui.setVisible(false);
			MainMenuUI.getInstance().setVisible(true);
			User.getInstance().setState(User.getInstance().SELECT_STATE);
		}

		@Override
		public void mousePressed(MouseEvent e) {
		}

		@Override
		public void mouseReleased(MouseEvent e) {
		}

		@Override
		public void mouseEntered(MouseEvent e) {
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}
	/**
	 * This method will get image.
	 * @throws IOException
	 */
	public void getImgage() throws IOException{
		for(File f : imagePath){
			if(!f.getName().startsWith(".")){
				Image image = ImageIO.read(f);
				imageArray.add(image);
			}
		}
	}

	/**
	 * Actionlistenr for key.
	 */
	class KeyControlListener implements KeyListener{

		@Override
		public void keyTyped(KeyEvent e) {
		}

		@Override
		public void keyPressed(KeyEvent e) {
			int key = e.getKeyCode();
			if(key == KeyEvent.VK_KP_LEFT  || key == KeyEvent.VK_LEFT){
				leftPageChange();
			}
			if(key == KeyEvent.VK_KP_RIGHT || key == KeyEvent.VK_RIGHT){
				rightPageChange();
			}
		}

		@Override
		public void keyReleased(KeyEvent e) {
		}

	}

	class LeftArrowActionListen implements MouseListener{

		@Override
		public void mouseClicked(MouseEvent e) {
			leftPageChange();
		}

		@Override
		public void mousePressed(MouseEvent e) {
		}

		@Override
		public void mouseReleased(MouseEvent e) {
		}

		@Override
		public void mouseEntered(MouseEvent e) {
		}

		@Override
		public void mouseExited(MouseEvent e) {
		}

	}

	class RightArrowActionListen implements MouseListener{

		@Override
		public void mouseClicked(MouseEvent e) {
			rightPageChange();
		}

		@Override
		public void mousePressed(MouseEvent e) {
		}

		@Override
		public void mouseReleased(MouseEvent e) {
		}

		@Override
		public void mouseEntered(MouseEvent e) {
		}

		@Override
		public void mouseExited(MouseEvent e) {
		}

	}

	/**
	 * This method will change page to right page.
	 */
	public void rightPageChange(){
		if(numPage < imageArray.size() - 1){
			numPage++;
			label.setIcon(new ImageIcon(imageArray.get(numPage)));
			scroller.getVerticalScrollBar().setValue(0);
		}

		labelNumPage.setText(String.format("Page : %-2d/%-2d ", (numPage+1), imageArray.size()));
	}

	/**
	 * This method will change page to left page.
	 */
	public void leftPageChange(){
		if(numPage > 0){
			numPage--;
			label.setIcon(new ImageIcon(imageArray.get(numPage)));
			scroller.getVerticalScrollBar().setValue(0);
		}
		labelNumPage.setText(String.format("Page : %-2d/%-2d ", (numPage+1), imageArray.size()));
	}

	public void run() throws IOException{
		this.initComponent();
		super.setVisible(true);
	}

}
