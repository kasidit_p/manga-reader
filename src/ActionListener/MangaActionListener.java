package ActionListener;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import downloader.DownloadInformation;
import Manga.Manga;
import UI.MainMenuUI;
import User.User;

/**
 * ActionListenr for Manga name to change to manga information.
 */
public class MangaActionListener implements MouseListener {
	
	private Manga manga;
	private JLabel label;
	private Boolean isShowSideBar;
	/**
	 * Constructor for this class.
	 * @param manga
	 * @param label
	 * @param isShownSideBar
	 */
	public MangaActionListener(Manga manga, JLabel label, Boolean isShownSideBar){
		this.manga = manga;
		this.label = label;
		this.isShowSideBar = isShownSideBar;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		User.getInstance().setState(User.getInstance().MANGAINFORMATION_STATE);
		User.getInstance().update();
		SwingUtilities.invokeLater( new Runnable( ) {
			public void run() {
				DownloadInformation information = new DownloadInformation(manga);
				information.execute();
			}
		});
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		if(isShowSideBar)
			MainMenuUI.panelSearch.setVisible(true);
		label.setForeground(Color.BLUE);
		MainMenuUI.instance.repaint();
		MainMenuUI.instance.pack();
	}

	@Override
	public void mouseExited(MouseEvent e) {
		if(isShowSideBar)
			MainMenuUI.panelSearch.setVisible(false);
		label.setForeground(Color.WHITE);
		MainMenuUI.instance.repaint();
		MainMenuUI.instance.pack();
	}

}