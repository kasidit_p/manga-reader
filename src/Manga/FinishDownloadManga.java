package Manga;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import downloader.DownloadManga;

/**
 * This class will look in your computer to find downlaoded manga.
 * @author Kasidit Phoncharoen and Sorawit Sakulkalanuwat
 *
 */
public class FinishDownloadManga {
	
	private List<String> name;
	private List<String> chapter;
	private File[] listManga;
	private File[] listChapter;
	private File[] imagePath;
	/**
	 * Constructor for this class.
	 */
	public FinishDownloadManga(){
		name = new ArrayList<String>();
		chapter = new ArrayList<String>();
	}
	
	/**
	 * Total page of manga that you downloaded. 
	 * @param file in your computer.
	 * @return number of total file.
	 */
	public int getTotalFile(File[] file){
		return file.length;
	}
	
	/**
	 * Total image file in computer.
	 * @param number of chapter that you want to read.
	 * @return Array of file.
	 */
	public File[] getImage(String number){
		for(File d : listChapter){
			if(d.getName().equalsIgnoreCase(number)){
				imagePath = d.listFiles();
			}
		}
		return imagePath;
	}
	
	/**
	 * This method will return list of chapter in selected manga.
	 * @param name of the manga
	 * @return List of chapter name.
	 */
	public List<String> getListChapter(String name){
		for(File f : listManga){
			if(f.getName().equalsIgnoreCase(name)){
				listChapter = null;
				listChapter = f.listFiles();
			}
		}
		chapter.clear();
		for(File c : listChapter){
			chapter.add(c.getName());
		}
		return chapter;
	}
	
	/**
	 * This method will return list of downloaded file.
	 * @return List of downlaoded name.
	 */
	public List<String> getDownloaded(){
		String path = DownloadManga.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		String directory = path.substring(0, path.lastIndexOf('/'))+"/MangaDownload";
		File dir = new File(directory);
		if(dir.canExecute()){
			listManga = dir.listFiles();
			for(File f : listManga ){
				if(f.getName().charAt(0) != '.')
					name.add(f.getName());
			}
		}
		return name;

	}
}
