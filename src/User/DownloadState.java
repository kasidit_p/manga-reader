package User;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import Manga.FinishDownloadManga;
import UI.MainMenuUI;
import UI.ReadMangaOfflineUI;

/**
 * This state change centerpanel in MainMenuUI to downloaded manga.
 * @author Kasidit Phoncharoen and Sorawit Sakulkalanuwat
 *
 */
public class DownloadState extends UserState{

	public DownloadState(User user) {
		super(user);

	}

	@Override
	public void updateUI() {
		MainMenuUI.getPanelCenter().removeAll();
		MainMenuUI.getPanelCenter().setBackground(Color.WHITE);
		FinishDownloadManga download = new FinishDownloadManga();
		List<String> nameList = download.getDownloaded();
		JPanel centerPanel = new JPanel();
		centerPanel.setLayout(new BoxLayout(centerPanel, BoxLayout.Y_AXIS));
		if(nameList.size() > 0)
			for(String n : nameList){
				JPanel panelWhite = new JPanel();
				panelWhite.setLayout(new BoxLayout(panelWhite, BoxLayout.Y_AXIS));
				panelWhite.setBackground(Color.WHITE);
				JPanel panel = new JPanel();
				//			panel.setBackground(Color.BLACK);
				JPanel panelName = new JPanel();
				panelName.setBackground(Color.BLACK);
				JLabel labelName = new JLabel(n);
				labelName.setFont(new Font("Verdana", Font.BOLD, 20));
				labelName.setForeground(Color.WHITE);
				panelName.add(labelName);
				for(String chapter : download.getListChapter(n)){
					JLabel labelChapter = new JLabel("Chapter "+ chapter);
					labelChapter.addMouseListener(new ChapterOfflineListener(download.getImage(chapter), labelChapter));
					panelWhite.add(labelChapter);
				}
				panel.add(panelName);
				panel.add(panelWhite);
				centerPanel.add(panel);
			}
		JScrollPane scrollPane = new JScrollPane(centerPanel, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED, ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		MainMenuUI.getPanelCenter().add(scrollPane);
		MainMenuUI.getInstance().pack();
		MainMenuUI.getInstance().repaint();

	}
	
	class ChapterOfflineListener implements MouseListener{
		File[] path;
		JLabel label;
		public ChapterOfflineListener(File[] path, JLabel label){
			this.path = path;
			this.label = label;
		}
		@Override
		public void mouseClicked(MouseEvent e) {
			MainMenuUI.getInstance().setVisible(false);
			ReadMangaOfflineUI ui = new ReadMangaOfflineUI(path, label.getText());
			try {
				ui.run();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}

		@Override
		public void mousePressed(MouseEvent e) {
		}

		@Override
		public void mouseReleased(MouseEvent e) {
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			label.setForeground(Color.BLUE);
		}

		@Override
		public void mouseExited(MouseEvent e) {
			label.setForeground(Color.BLACK);
		}
		
	}

}
