package User;
import UI.MainMenuUI;


/**
 * This state start MainMenuUI
 * @author Kasidit Phoncharoen and Sorawit Sakulkalanuwat
 *
 */
public class SelectMangaState extends UserState{

	public SelectMangaState(User user) {
		super(user);
	}

	public void updateUI() {
		MainMenuUI ui = MainMenuUI.getInstance();
		ui.run();
	}

}
