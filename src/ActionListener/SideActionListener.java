package ActionListener;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import UI.MainMenuUI;

/**
 * ActionListner to show side bar.
 */
public class SideActionListener implements MouseListener {

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}
	
	@Override
	public void mouseEntered(MouseEvent e) {	
		MainMenuUI.panelSearch.setVisible(true);
	}
	
	@Override
	public void mouseExited(MouseEvent e) {
		MainMenuUI.panelSearch.setVisible(false);
	}

}
