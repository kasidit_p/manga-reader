package User;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.List;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import ActionListener.MangaActionListener;
import Manga.Manga;
import UI.MainMenuUI;

/**
 * This state will change centerpanel in MainMenuUI to hotmanga.
 * @author Kasidit Phoncharoen and Sorawit Sakulkalanuwat
 *
 */
public class HotMangaState extends UserState{
	User user;
	public HotMangaState(User user) {
		super(user);
		this.user = user;
	}

	@Override
	public void updateUI() {
		SwingUtilities.invokeLater( new Runnable( ) {
			public void run() {
				DownloadImage thread = new DownloadImage();
				thread.execute();
			}
		});
	}

	class DownloadImage extends SwingWorker < Integer, Integer > implements Runnable{
		int countProgess = 0;
		int totalManga = 10;

		protected Integer doInBackground() throws Exception {
			MainMenuUI.getPanelCenter().removeAll();
			MainMenuUI.getPanelCenter().setBackground(Color.WHITE);
			MainMenuUI.getPanelCenter().setLayout(new BoxLayout(MainMenuUI.getPanelCenter(), BoxLayout.Y_AXIS));
			//			MainMenuUI.getPanelCenter().setBackground(Color.WHITE);
			countProgess++;
			publish(countProgess);
			List<String> popular = user.getURLManga().getPopularManga();
			for(int i = 0 ; i < totalManga ; i++){
				countProgess++;
				Manga manga = user.getURLManga().getManga(popular.get(i));
				JPanel panel = new JPanel();
				panel.setBackground(Color.BLACK);
				panel.setLayout(new GridLayout());
				countProgess++;
				JLabel labelName = new JLabel(i+1 + ". "+manga.getName());
				labelName.setFont(new Font("Calibri", Font.PLAIN, 20));
				labelName.addMouseListener(new MangaActionListener(manga ,labelName, false));
				panel.add(labelName);
				labelName.setForeground(Color.WHITE);
				JPanel panel2 = new JPanel();
				MainMenuUI.getPanelCenter().add(panel2);
				MainMenuUI.getPanelCenter().add(panel);
				publish(countProgess);
			}
			MainMenuUI.getInstance().pack();
			MainMenuUI.getInstance().repaint();
			return countProgess;

		}

		public void done(){
			MainMenuUI.getDownloadBar().setValue(0);
		}

		protected void process(List<Integer> chunks) {
			super.process(chunks);
			MainMenuUI.getDownloadBar().setMaximum(totalManga*2+1);
			MainMenuUI.getDownloadBar().setValue(chunks.get(chunks.size()-1));
		}

	}
}
