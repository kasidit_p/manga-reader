package ActionListener;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import UI.MainMenuUI;

/**
 * Actionlistner class for catalog label to  change to catalog page.
 */
public class CatalogActionListener implements MouseListener{

	private JLabel label;

	/**
	 * Constructor for this class.
	 */
	public CatalogActionListener(JLabel label){
		this.label = label;
	}
	
	/**
	 * When click change ui to catalog with alphabet.
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		MainMenuUI.getPanelCenter().removeAll();
		MainMenuUI.getPanelCenter().setLayout(new BorderLayout());
		JPanel panel = new JPanel();
		panel.setLayout(new FlowLayout());
		JLabel aLabel = new JLabel("A");
		aLabel.addMouseListener(new AlphabetActionListenr('A',aLabel));
		JLabel bLabel = new JLabel("B");
		bLabel.addMouseListener(new AlphabetActionListenr('B',bLabel));
		JLabel cLabel = new JLabel("C");
		cLabel.addMouseListener(new AlphabetActionListenr('C',cLabel));
		JLabel dLabel = new JLabel("D");
		dLabel.addMouseListener(new AlphabetActionListenr('D',dLabel));
		JLabel eLabel = new JLabel("E");
		eLabel.addMouseListener(new AlphabetActionListenr('E',eLabel));
		JLabel fLabel = new JLabel("F");
		fLabel.addMouseListener(new AlphabetActionListenr('F',fLabel));
		JLabel gLabel = new JLabel("G");
		gLabel.addMouseListener(new AlphabetActionListenr('G',gLabel));
		JLabel hLabel = new JLabel("H");
		hLabel.addMouseListener(new AlphabetActionListenr('H',hLabel));
		JLabel iLabel = new JLabel("I");
		iLabel.addMouseListener(new AlphabetActionListenr('I',iLabel));
		JLabel jLabel = new JLabel("J");
		jLabel.addMouseListener(new AlphabetActionListenr('J',jLabel));
		JLabel kLabel = new JLabel("K");
		kLabel.addMouseListener(new AlphabetActionListenr('K',kLabel));
		JLabel lLabel = new JLabel("L");
		lLabel.addMouseListener(new AlphabetActionListenr('L',lLabel));
		JLabel mLabel = new JLabel("M");
		mLabel.addMouseListener(new AlphabetActionListenr('M',mLabel));
		JLabel nLabel = new JLabel("N");
		nLabel.addMouseListener(new AlphabetActionListenr('N',nLabel));
		JLabel oLabel = new JLabel("O");
		oLabel.addMouseListener(new AlphabetActionListenr('O',oLabel));
		JLabel pLabel = new JLabel("P");
		pLabel.addMouseListener(new AlphabetActionListenr('P',pLabel));
		JLabel qLabel = new JLabel("Q");
		qLabel.addMouseListener(new AlphabetActionListenr('Q',qLabel));
		JLabel rLabel = new JLabel("R");
		rLabel.addMouseListener(new AlphabetActionListenr('R',rLabel));
		JLabel sLabel = new JLabel("S");
		sLabel.addMouseListener(new AlphabetActionListenr('S',sLabel));
		JLabel tLabel = new JLabel("T");
		tLabel.addMouseListener(new AlphabetActionListenr('T',tLabel));
		JLabel uLabel = new JLabel("U");
		uLabel.addMouseListener(new AlphabetActionListenr('U',uLabel));
		JLabel vLabel = new JLabel("V");
		vLabel.addMouseListener(new AlphabetActionListenr('V',vLabel));
		JLabel wLabel = new JLabel("W");
		wLabel.addMouseListener(new AlphabetActionListenr('W',wLabel));
		JLabel xLabel = new JLabel("X");
		xLabel.addMouseListener(new AlphabetActionListenr('X',xLabel));
		JLabel yLabel = new JLabel("Y");
		yLabel.addMouseListener(new AlphabetActionListenr('Y',yLabel));
		JLabel zLabel = new JLabel("Z");
		zLabel.addMouseListener(new AlphabetActionListenr('Z',zLabel));
		JPanel aToopanel = new JPanel();
		aToopanel.setLayout(new BoxLayout(aToopanel, BoxLayout.Y_AXIS));
		aToopanel.add(aLabel);
		aToopanel.add(bLabel);
		aToopanel.add(cLabel);
		aToopanel.add(dLabel);
		aToopanel.add(eLabel);
		aToopanel.add(fLabel);
		aToopanel.add(gLabel);
		aToopanel.add(hLabel);
		aToopanel.add(iLabel);
		aToopanel.add(jLabel);
		aToopanel.add(kLabel);
		aToopanel.add(lLabel);
		aToopanel.add(mLabel);
		aToopanel.add(nLabel);
		aToopanel.add(oLabel);
		aToopanel.add(pLabel);
		aToopanel.add(qLabel);
		aToopanel.add(rLabel);
		aToopanel.add(sLabel);
		aToopanel.add(tLabel);
		aToopanel.add(uLabel);
		aToopanel.add(vLabel);
		aToopanel.add(wLabel);
		aToopanel.add(xLabel);
		aToopanel.add(yLabel);
		aToopanel.add(zLabel);

		panel.add(aToopanel);
		MainMenuUI.getPanelCenter().add(panel, BorderLayout.WEST);
		MainMenuUI.getPanelCenter().setBackground(Color.BLACK);
		MainMenuUI.getInstance().pack();
		MainMenuUI.getInstance().repaint();
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		label.setIcon( new ImageIcon(ClassLoader.getSystemResource("images/Catalog2.jpg")));
	}

	@Override
	public void mouseExited(MouseEvent e) {
		label.setIcon( new ImageIcon(ClassLoader.getSystemResource("images/Catalog.jpg")));
	}

}