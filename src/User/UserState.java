package User;

/**
 * Interface of User State contain method updateUI
 * @author mac
 *
 */
public abstract class UserState { 
	protected User user; // clock model or controller
	public UserState(User user) { this.user = user; }

	public abstract void updateUI();

}
