package ActionListener;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import User.User;

/**
 * ActionListener for Download button.
 */
public class DownloadActionListener implements MouseListener {
	
	private JLabel label;
	
	/**
	 * Constructor for this class.
	 * @param label that want to change color.
	 */
	public DownloadActionListener(JLabel label){
		this.label = label;
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		User.getInstance().setState(User.getInstance().DOWNLOAD_STATE);
		User.getInstance().update();
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		label.setIcon(new ImageIcon(ClassLoader.getSystemResource("images/Download1.jpg")));
	}

	@Override
	public void mouseExited(MouseEvent e) {
		label.setIcon(new ImageIcon(ClassLoader.getSystemResource("images/Download.jpg")));
	}

}
