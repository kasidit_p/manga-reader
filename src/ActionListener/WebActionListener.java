package ActionListener;

import java.awt.Desktop;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 * ActioneListen for Credit to website mangareader.
 */
public class WebActionListener implements MouseListener{
	
	private final String URLPath = "http://www.mangareader.net/";
	private JLabel label;
	/**
	 * Constructor for this class.
	 * @param label
	 */
	public WebActionListener(JLabel label){
		this.label = label;
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		// open the default web browser for the HTML page
		try {
			Desktop.getDesktop().browse(new URI(URLPath));
		} catch (URISyntaxException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		label.setIcon(new ImageIcon(ClassLoader.getSystemResource("images/MangaWeb.png")));
	}

	@Override
	public void mouseExited(MouseEvent e) {
		label.setIcon(new ImageIcon(ClassLoader.getSystemResource("images/MangaWeb2.png")));
	}

}
