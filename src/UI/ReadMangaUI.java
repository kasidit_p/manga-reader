package UI;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.accessibility.AccessibleContext;
import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import Manga.Chapter;
import Manga.Manga;

/**
 * This class is read ui from given chapter.
 * @author Kasidit Phoncharoen and Sorawit Sakulkalanuwat
 *
 */
public class ReadMangaUI extends JFrame{

	private List<URL> listURL = new ArrayList<URL>();
	private int numPage = 0;
	private Chapter chapter;
	private JScrollPane scroller;
	private ReadMangaUI ui;
	private JLabel label;
	private JLabel labelNumPage;
	private JPanel panelTopMaster;
	private final int totalPage;
	private List<BufferedImage> imageArray;

	/**
	 * Constructor for this class.
	 * @param listURL of url.
	 * @param chapter of manga.
	 */
	public ReadMangaUI(List<URL> listURL,Chapter chapter)  {
		super("Image");
		this.listURL = listURL;
		this.chapter = chapter;
		this.ui = this;
		this.totalPage = listURL.size();
		imageArray = new ArrayList<BufferedImage>();
	}

	/**
	 * Component of this ui.
	 * @throws IOException if can't open url.
	 */
	public void initComponent() throws IOException{
		imageArray.add(ImageIO.read(chapter.getImageURL(listURL.get(0))));
		imageArray.add(ImageIO.read(chapter.getImageURL(listURL.get(1))));
		imageArray.add(ImageIO.read(chapter.getImageURL(listURL.get(2))));
		
		super.setLayout(new BoxLayout(super.getContentPane() , BoxLayout.Y_AXIS));

		panelTopMaster = new JPanel();
		panelTopMaster.setLayout(new FlowLayout(FlowLayout.LEFT));
		panelTopMaster.setBackground(Color.BLACK);
		JPanel panelTop = new JPanel();
		panelTop.setBackground(Color.BLACK);
		JPanel panelTop2 = new JPanel();
		panelTop2.setBackground(Color.BLACK);
		JPanel panelTop3 = new JPanel();
		panelTop3.setBackground(Color.BLACK);
		JPanel panelTop4 = new JPanel();
		panelTop4.setBackground(Color.BLACK);
		JPanel panelTop5 = new JPanel();
		panelTop5.setBackground(Color.BLACK);
		JPanel panelTop6 = new JPanel();
		panelTop6.setBackground(Color.BLACK);
		JLabel labelIcon = new JLabel(new ImageIcon(ClassLoader.getSystemResource("images/MangaReader3.png")));
		panelTop.add(labelIcon);
		JLabel labelHome = new JLabel(new ImageIcon(ClassLoader.getSystemResource("images/Home2.png")));
		labelHome.addMouseListener(new HomeActionListen());
		panelTop2.add(labelHome);
		JLabel labelHot = new  JLabel(new ImageIcon(ClassLoader.getSystemResource("images/Hot Manga2.png")));
		panelTop3.add(labelHot);
		JLabel labelLeftArrow = new  JLabel(new ImageIcon(ClassLoader.getSystemResource("images/Left-Arrow.png")));
		labelLeftArrow.addMouseListener(new LeftArrowActionListen());
		panelTop4.add(labelLeftArrow);
		JLabel labelRightArrow = new  JLabel(new ImageIcon(ClassLoader.getSystemResource("images/Right-Arrow.png")));
		labelRightArrow.addMouseListener(new RightArrowActionListen());
		panelTop5.add(labelRightArrow);
		labelNumPage = new JLabel(String.format("Page : %-2d/%-2d", (numPage+1), totalPage));
		labelNumPage.setForeground(Color.WHITE);
		labelNumPage.setFont(new Font("Tahoma", Font.BOLD, 60));
		panelTop6.add(labelNumPage);
		JPanel panelTop7 = new JPanel();
		panelTop7.setBackground(Color.BLACK);
		JLabel tempLabel = new JLabel("                ");
		panelTop7.add(tempLabel);

		panelTopMaster.add(panelTop);
		panelTopMaster.add(panelTop2);
		panelTopMaster.add(panelTop3);
		panelTopMaster.add(panelTop4);
		panelTopMaster.add(panelTop5);
		panelTopMaster.add(panelTop6);
		panelTopMaster.add(panelTop7);

		super.add(panelTopMaster);

		label = new JLabel(new ImageIcon(imageArray.get(0)));
		super.addKeyListener(new KeyControlListener());
		JPanel panel = new JPanel();
		panel.add(label);
		scroller = new JScrollPane(panel);
		scroller.setAutoscrolls(true);
		scroller.getVerticalScrollBar().setUnitIncrement(10);

		super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		super.add(scroller);
		super.pack();
		super.setLocation(200, 200);
		super.setVisible(true);
	}

	class KeyControlListener implements KeyListener{

		@Override
		public void keyTyped(KeyEvent e) {

		}

		@Override
		public void keyPressed(KeyEvent e) {
			int key = e.getKeyCode();
			if(key == KeyEvent.VK_KP_LEFT  || key == KeyEvent.VK_LEFT){
				leftPageChange();
			}
			if(key == KeyEvent.VK_KP_RIGHT || key == KeyEvent.VK_RIGHT){
				rightPageChange();
			}
		}

		@Override
		public void keyReleased(KeyEvent e) {
			// TODO Auto-generated method stub

		}

	}

	class HomeActionListen implements MouseListener{

		@Override
		public void mouseClicked(MouseEvent e) {
			ui.setVisible(false);
			MainMenuUI.getInstance().setVisible(true);
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

	}

	class LeftArrowActionListen implements MouseListener{

		@Override
		public void mouseClicked(MouseEvent e) {
			leftPageChange();
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

	}

	class RightArrowActionListen implements MouseListener{

		@Override
		public void mouseClicked(MouseEvent e) {
			rightPageChange();
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub

		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub

		}

	}

	/**
	 * This method will change page to right page.
	 */
	public void rightPageChange(){
		numPage++;
		if(numPage < totalPage)
		{
			if(numPage < imageArray.size())
			{
				label.setIcon(new ImageIcon(imageArray.get(numPage)));
				scroller.getVerticalScrollBar().setValue(0);
			}
			else
			{
				try 
				{
					imageArray.add(ImageIO.read(chapter.getImageURL(listURL.get(numPage))));
					label.setIcon(new ImageIcon(imageArray.get(numPage)));
					scroller.getVerticalScrollBar().setValue(0);
				} 
				catch (IOException e1) { e1.printStackTrace();}
			}
			labelNumPage.setText(String.format("Page : %-2d/%-2d ", (numPage+1), totalPage));
		}
	}

	/**
	 * This method will change page to left page.
	 */
	public void leftPageChange(){
		if(numPage > 0){
			numPage--;
			label.setIcon(new ImageIcon(imageArray.get(numPage)));
			scroller.getVerticalScrollBar().setValue(0);
		}
		labelNumPage.setText(String.format("Page : %-2d/%-2d ", (numPage+1), totalPage));
	}

	public void run() throws IOException{
		this.initComponent();
		super.setVisible(true);
	}

}