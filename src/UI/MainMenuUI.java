package UI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.LayoutManager;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import Manga.Manga;
import User.User;
import ActionListener.DownloadActionListener;
import ActionListener.HelpActionListener;
import ActionListener.HotMangActionListener;
import ActionListener.SearchActionListener;
import ActionListener.HomeActionListener;
import ActionListener.SideActionListener;
import ActionListener.WebActionListener;
import ActionListener.CatalogActionListener;

/**
 * This is the mainmenu ui. It will change the content inside when click at some label.
 * @author Kasidit Phoncharoen and Sorawit Sakulkalanuwat
 *
 */
public class MainMenuUI extends JFrame{
	public static final MainMenuUI instance = new MainMenuUI();
	private JPanel sidePanel;
	public static JPanel panelSearch;
	public JPanel outputLine;
	public JTextField text;
	public static ReadMangaUI readingUI;
	private static JProgressBar downloadBar;
	private static JPanel panelCenter;
	
	/**
	 * Constructor of this ui.
	 */
	public MainMenuUI(){
		super("Manga UI");
	}

	/**
	 * This method will get inastance of this class.
	 * @return insatnce.
	 */
	public static MainMenuUI getInstance(){
		return instance;
	}

	/**
	 * This is the component of this class.
	 */
	public void initComponent(){
		super.setPreferredSize(new Dimension(800,600));
		super.setLayout(new BorderLayout());

		JPanel panelTopMaster = new JPanel();
		panelTopMaster.setLayout(new FlowLayout(FlowLayout.LEFT,20,10));
		panelTopMaster.setBackground(Color.BLACK);
		JPanel panelTop = new JPanel();
		panelTop.setBackground(Color.BLACK);
		panelTop.setLayout(new FlowLayout(FlowLayout.LEFT));
		JPanel panelTop2 = new JPanel();
		panelTop2.setBackground(Color.BLACK);
		panelTop2.setLayout(new FlowLayout(FlowLayout.RIGHT,10,0));
		JPanel panelTop3 = new JPanel();
		panelTop3.setBackground(Color.BLACK);
		JLabel labelIcon = new JLabel(new ImageIcon(ClassLoader.getSystemResource("images/MangaReader3.png")));
		panelTop.add(labelIcon);
		JLabel labelHome = new JLabel(new ImageIcon(ClassLoader.getSystemResource("images/Home2.png")));
		labelHome.addMouseListener(new HomeActionListener());
		panelTop2.add(labelHome);
		JLabel labelHot = new  JLabel(new ImageIcon(ClassLoader.getSystemResource("images/Hot Manga2.png")));
		labelHot.addMouseListener(new HotMangActionListener(null));
		panelTop2.add(labelHot);
		JLabel labelReader = new JLabel(new ImageIcon(ClassLoader.getSystemResource("images/MangaWeb2.png")));
		labelReader.addMouseListener(new WebActionListener(labelReader));
		panelTop3.add(labelReader);
		panelTopMaster.add(panelTop);
		panelTopMaster.add(panelTop2);
		panelTopMaster.add(panelTop3);

		setPanelCenter(new JPanel());
		getPanelCenter().setBackground(Color.white);
		getPanelCenter().setLayout(new BoxLayout(getPanelCenter() ,BoxLayout.Y_AXIS));
		JPanel firstRow =new JPanel();
		firstRow.setBackground(Color.WHITE);
		firstRow.setLayout(new GridLayout());
		JLabel hotLabel = new JLabel(new ImageIcon(ClassLoader.getSystemResource("images/Hot.jpg")));
		firstRow.add(hotLabel);
		hotLabel.addMouseListener(new HotMangActionListener(hotLabel));
		JLabel catalogLabel = new JLabel(new ImageIcon(ClassLoader.getSystemResource("images/Catalog.jpg")));
		catalogLabel.addMouseListener(new CatalogActionListener(catalogLabel));
		firstRow.add(catalogLabel);
		JPanel secondRow =new JPanel();
		secondRow.setBackground(Color.WHITE);
		secondRow.setLayout(new GridLayout());
		JLabel downloadLabel = new JLabel(new ImageIcon(ClassLoader.getSystemResource("images/Download.jpg")));
		downloadLabel.addMouseListener(new DownloadActionListener(downloadLabel));
		secondRow.add(downloadLabel);
		JLabel helpLabel = new JLabel(new ImageIcon(ClassLoader.getSystemResource("images/Help.jpg")));
		helpLabel.addMouseListener(new HelpActionListener(helpLabel));
		secondRow.add(helpLabel);
		getPanelCenter().add(firstRow);
		getPanelCenter().add(secondRow);


		sidePanel = new JPanel();
		sidePanel.setBackground(Color.BLACK);
		sidePanel.setLayout(new BoxLayout(sidePanel, BoxLayout.Y_AXIS));
		panelSearch = new JPanel();
		panelSearch.setLayout(new BoxLayout(panelSearch,BoxLayout.PAGE_AXIS));
		panelSearch.setBackground(Color.BLACK);
		JPanel searchLine = new JPanel();
		searchLine.setBackground(Color.BLACK);
		text = new JTextField(10);
		text.setEditable(true);
		text.addMouseListener(new SideActionListener());
		text.addKeyListener(new SearchActionListener());
		searchLine.add(text);
		JLabel searchLabel = new JLabel(new ImageIcon(ClassLoader.getSystemResource("images/Browser.png")));
		searchLine.add(searchLabel);
		JLabel search = new JLabel(" ");
		outputLine = new JPanel();
		outputLine.setBackground(Color.BLACK);
		outputLine.setLayout(new BoxLayout(outputLine, BoxLayout.PAGE_AXIS));

		search.setBackground(Color.BLACK);
		panelSearch.add(searchLine);
		panelSearch.add(outputLine);
		panelSearch.setVisible(false);
		sidePanel.add(search);
		sidePanel.add(panelSearch);
		sidePanel.addMouseListener(new SideActionListener());

		JPanel panelBot = new JPanel();
		setDownloadBar(new JProgressBar());
		downloadBar.setStringPainted(true);
		panelBot.add(getDownloadBar());

		super.add(panelTopMaster, BorderLayout.NORTH);
		super.add(getPanelCenter(), BorderLayout.CENTER);
		super.add(panelBot, BorderLayout.SOUTH);
		super.add(sidePanel, BorderLayout.EAST);
		super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		super.pack();
	}

	/**
	 * Call this method when you to run this ui.
	 */
	public void run(){
		this.initComponent();
		super.setVisible(true);
	}

	/**
	 * This methid will return Panel in the center allow other to change content inside this panel. 
	 * @return PanelCenter
	 */
	public static JPanel getPanelCenter() {
		return panelCenter;
	}

	/**
	 * This method will set panel to the given panel.
	 * @param panelCenter
	 */
	public static void setPanelCenter(JPanel panelCenter) {
		MainMenuUI.panelCenter = panelCenter;
	}
	
	/**
	 * This method will return progressbar.
	 * @return downloadBar.
	 */
	public static JProgressBar getDownloadBar() {
		return downloadBar;
	}
	
	/**
	 * This allow to change downloadbar.
	 * @param downloadBar
	 */
	public static void setDownloadBar(JProgressBar downloadBar) {
		MainMenuUI.downloadBar = downloadBar;
	}
}
