package User;

import javax.swing.SwingUtilities;
import UI.LoadingScreen;

/**
 * Loading State use to set ui to LodingScrren
 * @author Kasidit Phoncharoen and Sorawit Sakulkalanuwat
 *
 */
public class LoadingState extends UserState{

	public LoadingState(User user) {
		super(user);
	}

	@Override
	public void updateUI() {
		LoadingScreen ui = LoadingScreen.getInstance();
		ui.run();
		SwingUtilities.invokeLater( new Runnable( ) {
			public void run() {
				try {
					user.getURLManga().execute();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}
	
}
