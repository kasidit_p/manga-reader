##Manga Read
###Member
1. Kasidit Phoncharoen 5710546151
2. Sorawit Sakulkalanuwat 5710546631

##Description

This program for download manga( japanese comic ) from http://www.mangareader.net
and you can open manga online or open from manga file that you download.

##Features

1. You can download manga pictures from our application by using URLReader.
2. You can read manga that you download in our application.
3. You can read manga direct from our application by using URL.
4. You can set favorite manga that you like.
5. You can search manga.

example picture:

![picture1](http://image.ohozaa.com/i/94f/nguPxO.png)
![picture2](http://image.ohozaa.com/i/d91/TG5GrF.jpg)
![picture3](http://image.ohozaa.com/i/16d/EUo3fi.jpg)
![picture4](http://image.ohozaa.com/i/ec6/lLtuAu.jpg)
![picture5](http://image.ohozaa.com/i/6c8/9kZA3j.jpg)
![picture6](http://image.ohozaa.com/i/1ec/lJHiWc.jpg)