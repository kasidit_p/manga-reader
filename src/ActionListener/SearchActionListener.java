package ActionListener;

import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

import javax.swing.JLabel;

import Manga.Manga;
import UI.MainMenuUI;
import User.User;

/**
 * ActionListener for search button.
 */
public class SearchActionListener implements KeyListener {

	@Override
	public void keyTyped(KeyEvent e) {
		String name = MainMenuUI.getInstance().text.getText();
		MainMenuUI.getInstance().outputLine.removeAll();
		List<Manga> listManga = User.getInstance().getURLManga().searchManga(name);
		for(int i = 0 ; i < listManga.size() ; i++){
			JLabel label = new JLabel(listManga.get(i).getName());
			label.setForeground(Color.WHITE);
			label.addMouseListener(new MangaActionListener(listManga.get(i), label, true));
			MainMenuUI.getInstance().outputLine.add(label);
		}
		if(listManga.size() < 25 ){
			for(int i = 0 ; i < 25 ; i++){
				JLabel label = new JLabel(" ");
				MainMenuUI.getInstance().outputLine.add(label);
			}
		}
		MainMenuUI.getInstance().pack();
		MainMenuUI.getInstance().repaint();

	}

	@Override
	public void keyPressed(KeyEvent e) {
	}

	@Override
	public void keyReleased(KeyEvent e) {
	}

}
