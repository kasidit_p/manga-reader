package User;

import Manga.URLManga;

/**
 * User state for changing page in ui.
 * @author Kasidit Phoncharoen and Sorawit Sakulkalanuwat
 *
 */
public class User {
	
	public UserState state;
	private static final User INSTANCE = new User(); 
	public final UserState SELECT_STATE = new SelectMangaState(this);
	public final UserState LOADING_STATE = new LoadingState(this);
	public final UserState HOTMANGA_STATE = new HotMangaState(this);
	public final UserState READING_STATE = new ReadingState(this);
	public final UserState MANGAINFORMATION_STATE = new MangaInformationState(this);
	public final UserState DOWNLOAD_STATE = new DownloadState(this);
	private URLManga urlManga ;
	
	/**
	 * Counstructor for this class. 
	 */
	public User(){
		state = LOADING_STATE;
		urlManga = URLManga.getInstance();
	}
	
	/**
	 * This mehtod return URLManga of this class.
	 * @return urlManga.
	 */
	public URLManga getURLManga(){
		return this.urlManga;
	}
	
	/**
	 * This mehtod return instance of User class.
	 * @return INSTANCE
	 */
	public static User getInstance() {
		return INSTANCE;
	}
	
	/**
	 * This method will set state of the user.
	 * @param state
	 */
	public void setState(UserState state){
		this.state = state;
	}
	
	/**
	 * This method return state of the user.
	 * @param state
	 */
	public UserState getState(){
		return this.state;
	}
	
	/**
	 * Update UI.
	 */
	public void update(){
		this.state.updateUI();
	}

}
