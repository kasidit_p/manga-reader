package Manga;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import javax.swing.SwingWorker;

import UI.LoadingScreen;
import User.User;

/**
 * This class get all manga from URL and put in mangaList.
 * @author 
 * 
 */
public class URLManga extends SwingWorker < Integer, Integer > implements Runnable {
	static URLManga instance = new URLManga(); 
	private URL url;
	private List<Manga> mangaList = new ArrayList<Manga>();
	int count;
	int totalLine;
	/**
	 * Constructor for this class. The url is http://www.mangareader.net/alphabetical .
	 * If there is no interet it will show the message. 
	 */
	public URLManga() {
		try {
			url = new URL("http://www.mangareader.net/alphabetical");
		} catch (MalformedURLException e) {
			System.out.println("Unable to connect to this website. Please check your internet.");
		}
		try {
			totalLine = this.countLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * This method will return instance of this class.
	 * @return INSTANCE
	 */
	public static URLManga getInstance(){
		return instance;
	}

	/**
	 * Get all manga from URL in constructor and put in mangList.
	 * Call this method after create this object. 
	 */
	public int getMangaFromURL() throws IOException{
		InputStream is = url.openStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		Stream<String> stream = reader.lines();
		Iterator<String> iter = stream.iterator();
		count = 0;
		while(iter.hasNext()){
			count++;
			publish(count);
			String line = iter.next();
			if(line.lastIndexOf("\">") < line.indexOf("</a>") && !line.contains("http")){
				String name = line.substring(line.lastIndexOf("\">")+2, line.indexOf("</a>"));
				String urlName = line.substring(line.indexOf("=\"")+2, line.indexOf("\">"));
				if(name.length() > 1 && urlName.length() > 1 && !name.contains("<") && urlName.contains("/")){
					mangaList.add(new Manga(name, new URL("http://www.mangareader.net"+urlName)));
				}
			}
			if(line.contains("mangacompleted")){
				String name = line.substring(line.indexOf("\"> ")+2, line.indexOf("</a>"));
				while(name.contains("\">")){
					name = name.substring(name.indexOf("\">")+2,name.length());
				}
				String urlName = line.substring(line.indexOf("href=\"")+6, line.indexOf("</a>")-name.length()-2);
				mangaList.add(new Manga(name, new URL("http://www.mangareader.net"+urlName)));
				mangaList.get(mangaList.size()-1).setCompleted(true);
			}

		}
		return count;
	}

	/**
	 * This method print all manga name and URL.
	 */
	public void printManga(){
		for(Manga m : mangaList){
			System.out.println(String.format("%-70s%-10s",m.getName(), m.getURL()));
		}
	}

	/**
	 * This method search manga by name.
	 * @param name of the manga that you want to search.
	 */
	public List<Manga> searchManga(String name){
		List<Manga> list = new ArrayList<>();
		if(name.length() > 1){
			for(Manga m : mangaList){
				if(m.getName().length() >= name.length()){
					char[] names = m.getName().toLowerCase().toCharArray();
					char[] arrayInput = name.toLowerCase().toCharArray();
					int countSame = 0;
					for(int i = 0 ; i < arrayInput.length ; i++){
						if(arrayInput[i] == names[i])
							countSame++;
					}
					if(countSame == arrayInput.length){
						list.add(m);
					}
				}
			}
		}
		return list;
	}

	/**
	 * Get manga from the given name.
	 * @param name of the manga that you want to get.
	 * @return if manga found return Manga that have given name. 
	 * 		   else return null.
	 */
	public Manga getManga(String name){
		for(Manga m : mangaList){
			if(m.getName().equalsIgnoreCase(name)){
				return m;
			}
		}
		return null;
	}

	/**
	 * Check if there is manga in our list.
	 * @param name that you want to check.
	 * @return if manga found return true.
	 * 		   else return false.
	 */
	public boolean containManga(String name){
		for(Manga m : mangaList){
			if(m.getName().equalsIgnoreCase(name)){
				return true;
			}
		}
		return false;
	}

	/**
	 * This method will count total line in mangareader.net/alphabetical
	 * @return total in mangareader.net/alphabetical
	 * @throws IOException if cant open URL
	 */
	public int countLine() throws IOException{
		InputStream is = url.openStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		Stream<String> stream = reader.lines();
		Iterator<String> iter = stream.iterator();
		int totalLine = 0;
		while(iter.hasNext()){
			totalLine++;
			iter.next();
		}
		return totalLine;
	}
	
	/**
	 * This method will get popular manga name.
	 * @return List of manga name.
	 */
	public List<String> getPopularManga(){
		InputStream is = null;
		List<String> listPopular = new ArrayList<String>();
		String url = "http://www.mangareader.net/popular";
		try {
			URL urlPopular = new URL(url);
			is = urlPopular.openStream();
		} catch (IOException e) {
			System.out.println("Can't open this manga URL. Please check the URL or internet connection.");
		}
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		Stream<String> stream = reader.lines();
		Iterator<String> iter = stream.iterator();
		while(iter.hasNext()){
			String line = iter.next();
			if( line.contains("<h3>") ){
				String name = line.substring( line.indexOf("\">") + 2, line.indexOf("</a>") );
				listPopular.add(name);
			}
		}
		return listPopular;
	}
	
	/**
	 * This method will return total manga in the website. 
	 * @return total manga that can read.
	 */
	public int getTotalManga(){
		return mangaList.size();
	}
	
	/**
	 * This method will get manga by given alphabet
	 * @param alphabet that you want to get.
	 * @return List of manga.
	 */
	public List<Manga> getMangaByAlphabet(char alphabet){
		List<Manga> searchList = new ArrayList<Manga>();
		for(Manga m : mangaList){
			if(m.getName().charAt(0) == alphabet){
				searchList.add(m);
			}
		}
		return searchList;
	}
	
	protected Integer doInBackground() throws Exception {
		int num = this.getMangaFromURL();
		return num;
	}

	public void done(){
		LoadingScreen ui = LoadingScreen.getInstance();
		ui.setVisible(false);
		User.getInstance().setState(User.getInstance().SELECT_STATE);
		User.getInstance().update();
	}

	protected void process(List<Integer> chunks) {
		super.process(chunks);
		LoadingScreen.getBar().setMaximum(totalLine);
		LoadingScreen.getBar().setValue(chunks.get(chunks.size()-1));
	}

}
