package Manga;

import User.User;

/**
 * Main class for running application.
 * @author Kasidit Phoncharoen and Sorawit Sakulkalanuwat
 *
 */
public class Main {
	
	public static void main(String[] args) {
		User user = new User();
		user.state.updateUI();
	}
	
}
