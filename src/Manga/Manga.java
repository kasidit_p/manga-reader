package Manga;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

/**
 * Manga class contain manga's name, manga's url and boolean of comlete.
 * @author mac
 *
 */
public class Manga {
	private String name;
	private URL urlManga;
	private Boolean completed;
	/** 
	String alternateName;
	String releaseYear;
	String author;
	String artist;
	String mangaSummary;
	 */
	private List<Chapter> chapterList;

	/**
	 * Constructor for this class.
	 * @param name of the manga
	 * @param url of the manga.
	 */
	public Manga(String name, URL urlManga) {
		this.name = name;
		this.urlManga = urlManga;
		this.setCompleted(false);
		chapterList = new ArrayList<Chapter>();
	}

	/**
	 * Get manga name of this class.
	 * @return Manga's name.
	 */
	public String getName(){
		return name;
	}

	/**
	 * Get URL from this class.
	 * @return Manga's URL.
	 */
	public URL getURL(){
		return urlManga;
	}

	/**
	 * Method equal check whether this and the given object have the same name or not.
	 * @param obj that you want to check. 
	 */
	public boolean equals(Object obj){
		if(obj == null)
			return false;
		if(this.getClass() != obj.getClass())
			return false;
		Manga temp = (Manga) obj;
		if(temp.getName().equalsIgnoreCase(this.getName()))
			return true;
		else
			return false;
	}

	/**
	 * This method return the boolean of completed/
	 * @return completed.
	 */
	public Boolean getCompleted() {
		return completed;
	}

	/**
	 * Set completed to the given boolean.
	 * @param completed
	 */
	public void setCompleted(Boolean completed) {
		this.completed = completed;
	}

	/**
	 * This method get total chapter of this manga.
	 * @return total chapter number of this manga.
	 */
	public int getTotalChapter() {
		int total = 0;
		this.getChapters();
		String name = chapterList.get( chapterList.size() - 1 ).getURL().getPath();
		if(name.contains("chapter")){
			total = Integer.parseInt( name.substring( name.indexOf("chapter-") + 8, name.indexOf(".html") ) );
		}
		else {
			total =  Integer.parseInt( name.substring( name.lastIndexOf("/") + 1, name.length() ) );
		}
		return total;
	}

	/**
	 * This method get all the chapter from this Manga's URL.
	 * If there is no internet connection it will show warning message.
	 */
	public void getChapters() {
		InputStream is = null;
		try {
			is = urlManga.openStream();
		} catch (IOException e) {
			System.out.println("Can't open this manga URL. Please check the URL or internet connection.");
		}
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		Stream<String> stream = reader.lines();
		Iterator<String> iter = stream.iterator();
		List<String> list = new ArrayList<String>();
		while(iter.hasNext()){
			String line = iter.next();
			if(line.contains("chapterlist") || (list.size() > 0)){
				list.add(line);
			}
			if(list.size() > 0 && line.contains("</table>")){
				break;
			}
		}
		for(String a : list){
			if(a.contains("href")){
				String url = a.substring(a.indexOf("/"),a.indexOf("\">"));
				String number = a.substring(a.indexOf(this.name)+this.name.length()+1,a.indexOf("</"));
				String name = a.substring(a.indexOf(" : ")+3,a.indexOf("</td>"));
				try {
					chapterList.add(new Chapter(name,number,new URL("http://www.mangareader.net"+url)));
				} catch (MalformedURLException e) {
					System.out.println("Can't open this chapter URL. Please check the URL or internet connection.");
				}
			}
		}
	}

	/**
	 * This method return Chapter to the given number.
	 * @param number of the chapter.
	 * @return Chapter 
	 */
	public Chapter getChapter(int number) {
		for(Chapter a : chapterList){
			if( a.getURL().getPath().contains( "chapter-" + number ) ){
				return a;
			}
			else if( a.getURL().getPath().contains( "/" + number ) ){
				return a;
			}
		}
		return chapterList.get( number - 1 );
	}

	/**
	 * This method get summary of this manga. 
	 * @return summary of this manga.
	 */
	public String getSummary() {
		String mangaSummary = "";
		InputStream is = null;
		try {
			is = urlManga.openStream();
		} catch (IOException e) {
			System.out.println(name);
		}
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		String line;
		try {
			while((line = reader.readLine()) != null){
				if(line.contains("<p>")){
					while(!mangaSummary.contains("</p>")){
						mangaSummary+=line;
						line = reader.readLine();
					}
					mangaSummary = mangaSummary.replace("<p>", "");
					mangaSummary = mangaSummary.replace("</p>", "");
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return mangaSummary;
	}
	
	/**
	 * This method will return URL of cover image.
	 * @return URL of cover image.
	 */
	public URL getCoverURL(){
		InputStream is = null;
		try {
			is = urlManga.openStream();
		} catch (IOException e) {
			System.out.println("Can't open this manga URL. Please check the URL or internet connection.");
		}
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		Stream<String> stream = reader.lines();
		Iterator<String> iter = stream.iterator();
		while(iter.hasNext()){
			String line = iter.next();
			if(line.contains("mangaimg")){
				String url = line.substring( line.indexOf("src=") + 5, line.indexOf("jpg") + 3);
				try {
					return new URL(url);
				} catch (MalformedURLException e) {
					System.out.println("Can't open this Cover url.");
				}
			}
		}
		return null;
	}
	
	/**
	 * This method will return List of chapter name in this manga.
	 * @return List of chapter name.
	 */
	public List<Chapter> getChapterList(){
		return this.chapterList;
	}

}
