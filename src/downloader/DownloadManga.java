package downloader;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

import Manga.Chapter;
import Manga.Manga;
import UI.MainMenuUI;

/**
 * This class will download selected manga into your computer. 
 * @author Kasidit Phoncharoen and Sorawit Sakulkalanuwat
 *
 */
public class DownloadManga extends SwingWorker < Integer, Integer > implements Runnable{
	private List<URL> urlList;
	private Manga manga;
	private Chapter chapter;
	/**
	 * Constructor for this class.
	 * @param manga that you want to download.
	 * @param chapter of that manga that you want to downlaod.
	 */
	public DownloadManga(Manga manga, Chapter chapter){
		try {
			this.urlList = chapter.getURLList();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.manga = manga;
		this.chapter = chapter;
	}
	/**
	 * This method will download the selected chapter into your computer.
	 * The path is this workspace.
	 * @return process of download.
	 * @throws IOException if the file can't download
	 */
	public Integer download() throws IOException{
		String path = DownloadManga.class.getProtectionDomain().getCodeSource().getLocation().getPath();
		String directory = path.substring(0, path.lastIndexOf('/'));
		File dir = new File(directory+"/MangaDownload"+"/"+manga.getName()+"/"+chapter.getNumChapter());
		if(!dir.exists()){
			dir.mkdirs();
		}
		int page = 0;
		for(URL u : urlList){
			page++;
			publish(page);
			File filename = File.createTempFile(String.format("%03dPage" ,page) , ".jpg", dir);
			URLReader reader = null;
			try {
				reader = new URLReader( chapter.getImageURL(u), filename );
			} catch (IOException e) {
				System.out.println("Download Fail "+page);
			}
			// download the URL
			reader.execute();
		}
		return page;
	}
	
	/**
	 * This method will do download in background.
	 */
	protected Integer doInBackground() throws Exception {
		return download();
	}
	
	/**
	 * When done the download bar will show chapter finish.
	 */
	public void done(){
		JProgressBar bar = MainMenuUI.getDownloadBar();
		bar.setString(String.format("%s", "Done chapter "+chapter.getNumChapter()));
	}
	
	/**
	 * Doing download this will set download bar text and value.
	 */
	protected void process(List<Integer> chunks) {
		super.process(chunks);
		JProgressBar bar = MainMenuUI.getDownloadBar();
		bar.setMaximum(urlList.size());
		bar.setValue(chunks.get(chunks.size()-1));
		bar.setString(chunks.get(chunks.size()-1) + " / "+ urlList.size());
	}

}
