package ActionListener;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.net.URL;
import java.util.List;

import Manga.Chapter;
import UI.ReadMangaUI;
import User.User;

/**
 * Actionlistener for read button.
 */
public class ReadActionListener implements MouseListener {

	private Chapter chapter;
	/**
	 * Constructor for this class.
	 * @param manga
	 * @param chapter
	 */
	public ReadActionListener(Chapter chapter){
		this.chapter = chapter;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		User.getInstance().setState(User.getInstance().READING_STATE);
		User.getInstance().update();
		System.out.println(chapter.getName());
		try {
			List<URL> urlList = chapter.getURLList();
			ReadMangaUI readingUI = new ReadMangaUI(urlList, chapter);
			readingUI.run();
		} catch (IOException e2) {
			e2.printStackTrace();
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
	}

	@Override
	public void mouseReleased(MouseEvent e) {
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

}