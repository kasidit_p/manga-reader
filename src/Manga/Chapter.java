package Manga;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

/**
 * This class will store chapter name, number of that chapter, url to that shpter.
 * @author Kasidit Phoncharoen and Sorawit Sakulkalanuwat
 *
 */
public class Chapter {
	private String name;
	private String numChapter;
	private URL url;
	private List<URL> pages;
	private List<URL> imagesList;
	
	/**
	 * Constructor for this class.
	 * @param name of the chapter.
	 * @param numChapter of the chapter.
	 * @param url of the chapter.
	 */
	public Chapter(String name, String numChapter, URL url) {
		this.name = name;
		this.numChapter = numChapter;
		this.url = url;
		pages = new ArrayList<URL>();
		imagesList  = new ArrayList<URL>();
	}
	
	/**
	 * This method will return name of the chapter.
	 * @return name of the chapter.
	 */
	public String getName(){
		return name;
	}
	
	/**
	 * This method will return number of the chapter.
	 * @return numChapter
	 */
	public String getNumChapter() {
		return numChapter;
	}

	/**
	 * This method will return url of the chapter
	 * @return url
	 */
	public URL getURL(){
		return url;
	}
	
	/**
	 * This method will count to page of manga in that chapter.
	 * @return number of total page.
	 * @throws IOException if page can't open
	 */
	public int getTotalPages() throws IOException{
		URL urlChapter = new URL(url.toString());
		InputStream is = urlChapter.openStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		Stream<String> stream = reader.lines();
		Iterator<String> iter = stream.iterator();
		int number = 0;
		while(iter.hasNext()){
			String line = iter.next();
			if(line.contains("</select> ")){
				number = Integer.parseInt(line.substring(13, line.indexOf("</div>")));
				return number;
			}
		}
		return number;
	}
	
	/**
	 * This method will return list of url in that chapter.
	 * @return List of url.
	 * @throws IOException if url can't open.
	 */
	public List<URL> getURLList() throws IOException{
		pages.add(url);
		int total = this.getTotalPages();
		for(int i = 0 ; i < total ; i++){
			if(i != 0){
				if(!url.toString().contains("chapter"))
					pages.add(new URL(url.toString()+"/"+(i+1)));
				else{
					String path = "";
					String patternString = "(\\d+)-(\\d+)-";
					Pattern pattern = Pattern.compile(patternString);
					Matcher matcher = pattern.matcher(url.toString());
					if(matcher.find()){
						path = matcher.group() + (i + 1);
						String newURL = url.toString().replaceFirst("(\\d+)-(\\d+)-(\\d+)", path);
						pages.add(new URL(newURL));
					}else{
						System.out.println("regex not found!!!");
					}
				}
			}
		}
		return pages;
	}
	
	/**
	 * This method will get image url from given url page.
	 * @param page that you want to get image.
	 * @return url of image.
	 * @throws IOException if can't open url.
	 */
	public URL getImageURL(URL page) throws IOException{
		URL urlPage = new URL(page.toString());
		InputStream is = urlPage.openStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		Stream<String> stream = reader.lines();
		Iterator<String> iter = stream.iterator();
		while(iter.hasNext()){
			String line = iter.next(); 
			if(line.contains("<img ")){
				String url = line.substring(line.indexOf("src")+5,line.indexOf("jpg")+3);
				return new URL(url);
			}
		}
		return null;
	}
	
	/**
	 * This method list of image url in this chapter.
	 * @return List of image url.
	 * @throws IOException if can't open url.
	 */
	public List<URL> getImagesURLList() throws IOException{
		if(pages.size() == 0)
			this.getURLList();
		for(URL u : pages){
			imagesList.add(this.getImageURL(u));
		}
		return imagesList;
	}
	
}
