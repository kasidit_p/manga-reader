package UI;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

/**
 * This is loading ui when you start programe.
 * @author Kasidit Phoncharoen and Sorawit Sakulkalanuwat
 *
 */
public class LoadingScreen extends JFrame{
	
	private static JProgressBar bar;
	public static final LoadingScreen instance = new LoadingScreen();
	
	/**
	 * Construtor for this ui.
	 */
	public LoadingScreen(){
		super("Loading");
		super.setUndecorated(true);
	}
	
	/**
	 * Component in the ui.
	 */
	public void initComponent(){
		JPanel panel  = new JPanel();
		panel.setBackground(Color.BLACK);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		JLabel labelScreen = new JLabel(new ImageIcon(ClassLoader.getSystemResource("images/Preload.png")));
		panel.add(labelScreen);
		setBar(new JProgressBar());
		panel.add(getBar());
		super.add(panel);
		super.pack();
		super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		super.setLocation(dim.width/2-super.getSize().width/2, dim.height/2-super.getSize().height/2);
	}
	
	/**
	 * Call this method when you want to start ui.
	 */
	public void run(){
		this.initComponent();
		this.setVisible(true);
	}
	
	/**
	 * This method will get inastance of this methods.
	 * @return instance
	 */
	public static LoadingScreen getInstance(){
		return instance;
	}
	
	/**
	 * This return progressbar of this ui.
	 * @return
	 */
	public static JProgressBar getBar() {
		return bar;
	}
	
	/**
	 * This will set progess bar to given bar.
	 * @param bar
	 */
	public static void setBar(JProgressBar bar) {
		bar.setPreferredSize(new Dimension(0,20));
		bar.setForeground(Color.GREEN);
		LoadingScreen.bar = bar;
	}
	
}
